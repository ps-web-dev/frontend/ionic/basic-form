const button = document.querySelector('ion-button');
const input = document.querySelector('ion-input');
const outputPara = document.querySelector('p');

button.addEventListener('click', () => {
    const enteredValue = input.value;
    if( enteredValue.trim().length <= 0 || !enteredValue) {
        return;
    }
    outputPara.textContent = enteredValue;
    input.value = '';
})